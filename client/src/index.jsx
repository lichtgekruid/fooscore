import React from 'react';
import ReactDOM from 'react-dom';
import WebfontLoader from '@dr-kobros/react-webfont-loader';
import { BrowserRouter, Route } from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './index.css';

ReactDOM.render(
  <WebfontLoader
    config={{
      google: {
        families: ['Montserrat:400,600,700,800', 'Open Sans: 800,700'],
      },
    }}
  >
    <BrowserRouter
      forceRefresh={false}
    >
      <LastLocationProvider>
        <Route component={App} />
      </LastLocationProvider>
    </BrowserRouter>
  </WebfontLoader>,
  document.querySelector('.root'),
);
serviceWorker.register();
