const knex = require('libs/knex');

module.exports = async (playerNames) => {
  const results = (await knex.raw(`
  SELECT 
         name,
    SUM((SELECT sum(match_scores.result) AS sum
           FROM match_scores
          WHERE (match_scores.team_id = team_players.team_id)
          GROUP BY match_scores.team_id)) AS "totalScore"
    FROM players
    JOIN team_players ON players.id = team_players.player_id
GROUP BY players.id, name
  HAVING name = ANY(?)
  `, [playerNames])).rows;

  return results.map(result => ({
    ...result, totalScore: parseInt(result.totalScore, 10),
  }));
};
