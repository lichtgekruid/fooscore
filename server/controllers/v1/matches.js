
/* jshint esversion: 6 */
/* globals require, module */
const express = require('express');
const validator = require('express-validator/check');

const router = express.Router({ mergeParams: true });
const teamsRepo = require('repositories/teams');
const playersRepo = require('repositories/players');
const matchesRepo = require('repositories/matches');

router.post('/', [
  validator.body('score1')
    .isInt({ min: 0 })
    .withMessage('Score 1 can only be a number above or equal to 0.')
    .toInt(),
  validator.body('score2')
    .isInt({ min: 0 })
    .withMessage('Score 2 can only be a number above or equal to 0.')
    .toInt(),
  validator.body('team1')
    .isArray()
    .withMessage('Team 1 needs to be an array.'),
  validator.body('team2')
    .isArray()
    .withMessage('Team 2 needs to be an array.'),
], async (req, res) => {
  const validation = validator.validationResult(req);

  if (!validation.isEmpty()) {
    const errors = validation.array();
    return res.status(422).json({
      status: 'fail',
      type: 'VALIDATION_ERROR',
      messages: errors.map(error => error.msg),
    });
  }

  const {
    team1, team2, score1, score2,
  } = req.body;

  const team1Players = await playersRepo.getOrCreate(team1);
  const team2Players = await playersRepo.getOrCreate(team2);

  const team1Id = await teamsRepo.getOrCreate(team1Players);
  const team2Id = await teamsRepo.getOrCreate(team2Players);

  let result1;
  let result2;
  if (score1 > score2) {
    result1 = 2;
    result2 = 0;
  } else if (score1 < score2) {
    result1 = 0;
    result2 = 2;
  } else {
    result1 = 1;
    result2 = 1;
  }

  await matchesRepo.post({
    team1Id, team2Id, score1, score2, result1, result2,
  });

  return res.status(200).json({
    status: 'success',
  });
});

module.exports = router;
