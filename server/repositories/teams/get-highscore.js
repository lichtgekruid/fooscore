const knex = require('libs/knex');

module.exports = async () =>
  (await knex('team_stats')
    .select('total_score')
    .where('rank', 1)
    .pluck('total_score'))[0];

