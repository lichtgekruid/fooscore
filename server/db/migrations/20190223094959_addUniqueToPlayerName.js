exports.up = (knex, Promise) => Promise.all([
  knex.schema.alterTable('players', (table) => {
    table.unique('name');
  }),
]);

exports.down = (knex, Promise) => Promise.all([
  knex.schema.alterTable('players', (table) => {
    table.dropUnique('name');
  }),
]);
