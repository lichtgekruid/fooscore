/* jshint esversion: 6 */
/* globals require, module */
const express = require('express');
const validator = require('express-validator/check');

const router = express.Router({ mergeParams: true });
const teamsRepo = require('repositories/teams');

router.get('/', async (req, res) => {
  let rankings;
  let highscore;
  try {
    rankings = (await teamsRepo.get())
      .map(result => ({
        ...result, totalScore: parseInt(result.totalScore, 10),
      }));
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err });
  }

  try {
    highscore = parseInt((await teamsRepo.getHighscore()), 10);
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err });
  }

  return res.status(200).json({ status: 'success', data: { rankings, highscore } });
});


router.patch('/:id', [
  validator.body('name')
    .isAscii()
    .withMessage('Name needs to be a valid Ascii string.')
    .isLength({ min: 3 })
    .withMessage('Teamname needs to be at least 3 characters long'),
  validator.param('id')
    .isInt()
    .withMessage('Id needs to be an int.')
    .toInt(),
], async (req, res) => {
  try {
    const { name } = req.body;
    const { id } = req.params;
    await teamsRepo.patchName({ id, name });
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err });
  }

  return res.status(200).json({ status: 'success' });
});

module.exports = router;
