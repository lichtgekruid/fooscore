exports.up = (knex, Promise) => Promise.all([
  knex.schema.createTable('players', (table) => {
    table.increments();
    table.string('name');
  }),
]);

exports.down = (knex, Promise) =>
  Promise.all([
    knex.schema.dropTable('players'),
  ]);
