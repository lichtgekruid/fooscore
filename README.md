## Requirements
- Docker
- Docker-compose

## Installation steps
- Add these 2 lines to your /etc/hosts file:
```
127.0.0.1       api.fooscore.local
127.0.0.1	fooscore.local
```

- In a terminal, go to the project directory
- Enter `docker-compose up --build`

## Not implemented
- Amount of close victories per team
- Notifications do not dissapear automatically
- When adding a playername, it could show those that are already in the system
- When adding a playername, it could show teams that are already in the system
- Tests
- Production environment