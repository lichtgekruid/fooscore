import React from 'react';
import PropTypes from 'prop-types';

import './ranking.css';
import TeamName from './TeamName';

function ScoreGraph({ score, highscore }) {
  let percentage = `${(score * 100) / highscore}%`;
  if (percentage === '0%') percentage = '.5%';
  return (
    <div className="score-bar__container">
      <div
        className="ranking__score-bar"
        style={{ width: percentage }}
      />
    </div>
  );
}

function Ranking({
  rankings, highscore, showNotification, updateDashboard,
}) {
  return (
    <div className="ranking">
      <h2>Ranks</h2>
      <table>
        { (rankings.length !== 0 && (
          <thead>
            <tr>
              <th className="rankings__rank">Rank</th>
              <th className="rankings__team">Team</th>
              <th>Played</th>
              <th className="desktop">Wins</th>
              <th className="desktop">Draws</th>
              <th className="desktop">Losses</th>
              <th className="rankings__score">Score</th>
              <th />
            </tr>
          </thead>
        ))}
        <tbody>
          { rankings.map(ranking => (
            <tr key={`${ranking.rank}-${ranking.name}`}>
              <td className="rankings__rank">
                <div className="rank">
                  {ranking.rank}
                </div>
              </td>
              <TeamName
                name={ranking.name}
                id={ranking.id}
                showNotification={showNotification}
                updateDashboard={updateDashboard}
              />
              <td className="rankings__played">{ranking.gamesPlayed}</td>
              <td className="desktop rankings__won">{ranking.gamesWon}</td>
              <td className="desktop rankings__drawn">{ranking.gamesDrawn}</td>
              <td className="desktop rankings__lost">{ranking.gamesLost}</td>
              <td className="rankings__score">{ranking.totalScore}</td>
              <td>
                <ScoreGraph
                  highscore={highscore}
                  score={ranking.totalScore}
                />
              </td>
            </tr>
          ))}
          { (rankings.length === 0 && (
            <tr>
              <td colSpan="7" className="rankings--empty">No matches were added yet!</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

Ranking.propTypes = {
  rankings: PropTypes.array.isRequired,
  highscore: PropTypes.number,
  updateDashboard: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
};

Ranking.defaultProps = {
  highscore: 0,
};

ScoreGraph.propTypes = {
  score: PropTypes.number.isRequired,
  highscore: PropTypes.number.isRequired,
};

export default Ranking;
