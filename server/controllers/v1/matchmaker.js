/* jshint esversion: 6 */
/* globals require, module */
const express = require('express');
const validator = require('express-validator/check');

const router = express.Router({ mergeParams: true });
const playersRepo = require('repositories/players');

router.post('/', [
  validator.body('playerNames')
    .isArray()
    .withMessage('Playernames needs to be an array'),
], async (req, res) => {
  const validation = validator.validationResult(req);

  if (!validation.isEmpty()) {
    const errors = validation.array();
    return res.status(422).json({
      status: 'fail',
      type: 'VALIDATION_ERROR',
      messages: errors.map(error => error.msg),
    });
  }
  const { playerNames } = req.body;
  if (playerNames.length < 2) {
    return res.status(422).json({
      status: 'fail',
      type: 'VALIDATION_ERROR',
      messages: 'Playernames must be bigger than 2',
    });
  }

  let knownPlayers;
  try {
    knownPlayers = await playersRepo.getByNames(playerNames);
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err.message });
  }

  // make sure all players have a score
  let playersWithScores;

  if (playerNames.length === knownPlayers.length) playersWithScores = knownPlayers;
  else if (knownPlayers.length === 0) {
    playersWithScores = playerNames
      .map(name => ({ name, totalScore: 1 }));
  } else {
    const totalScoreAll = knownPlayers.reduce((a, { totalScore }) => a + totalScore, 0);
    const averageScore = Math.floor(totalScoreAll / knownPlayers.length);
    const unknownPlayers = playerNames
      .filter((name) => {
        const knownPlayer = knownPlayers.filter(player => (player.name === name));
        return knownPlayer.length === 0;
      });
    const scoredUnknownPlayers = unknownPlayers
      .map(player => ({ name: player, totalScore: averageScore }));
    playersWithScores = knownPlayers.concat(scoredUnknownPlayers);
  }

  // order high to low score
  playersWithScores.sort((a, b) => {
    if (a.totalScore > b.totalScore) return -1;
    if (a.totalScore < b.totalScore) return 1;
    return 0;
  });

  // iterate through players and add them to lowest pile
  const team1 = [];
  const team2 = [];
  playersWithScores.forEach((player) => {
    const totalScoreTeam1 = team1.reduce((a, { totalScore }) => a + parseInt(totalScore, 10), 0);
    const totalScoreTeam2 = team2.reduce((a, { totalScore }) => a + parseInt(totalScore, 10), 0);
    if (totalScoreTeam1 > totalScoreTeam2) team2.push(player);
    else team1.push(player);
  });

  const team1Names = team1.map(player => player.name);
  const team2Names = team2.map(player => player.name);

  return res.status(200).json({
    status: 'success',
    data: {
      team1: team1Names,
      team2: team2Names,
    },
  });
});

module.exports = router;
