exports.up = (knex, Promise) => Promise.all([
  knex.schema.createTable('team_players', (table) => {
    table.increments();
    table.integer('team_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('teams');

    table.integer('player_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('players');
  }),
]);

exports.down = (knex, Promise) =>
  Promise.all([
    knex.schema.dropTable('team_players'),
  ]);
