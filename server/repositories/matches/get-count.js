const knex = require('libs/knex');

module.exports = async () => {
  const oneWeekAgo = new Date();
  oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);

  const { count } = await knex('matches')
    .count('id')
    .where('created_on', '>', oneWeekAgo)
    .first();

  return parseInt(count, 10);
};
