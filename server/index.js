/* jshint esversion: 6 */
/* globals require, process */
// require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const cors = require('./middlewares/cors');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors);

app.use('/v1', require('./controllers/v1'));

app.use((req, res) => {
  res.status(404);
  if (req.accepts('json')) {
    res.send({ status: 'fail', type: 'NOT_FOUND', message: 'Not found' });
    return;
  }

  res.type('txt').send('Not found');
});

app.listen(process.env.APP_PORT);
console.log(`Your server is running on port ${process.env.APP_PORT}.`);
