import React, { Component } from 'react';

import PropTypes from 'prop-types';
import './match-form.css';
import Actions from './Actions';
import Players from './Players';

class MatchForm extends Component {
  constructor(props) {
    super(props);

    this.addPlayerHandler = this.addPlayerHandler.bind(this);
    this.scoreChangeHandler = this.scoreChangeHandler.bind(this);
    this.removePlayerHandler = this.removePlayerHandler.bind(this);
    this.submitMatchHandler = this.submitMatchHandler.bind(this);
    this.verifyMatch = this.verifyMatch.bind(this);
    this.setTeams = this.setTeams.bind(this);

    this.state = {
      score1: '',
      score2: '',
      team1: [],
      team2: [],
    };
  }

  setTeams(team1, team2) {
    this.setState({ team1, team2 });
  }

  addPlayerHandler(props) {
    const { team1, team2 } = this.state;
    const { id } = props;
    let { value } = props;
    const { showNotification } = this.props;

    value = value.trim();
    if (team1.indexOf(value) !== -1
    || team2.indexOf(value) !== -1) {
      showNotification('Cannot add a player name to a match twice', true);
      return;
    }

    if ((id === 'team1' && team1.length > 3)
      || (id === 'team2' && team2.length > 3)) {
      showNotification('Team is full', true);
      return;
    }
    if (id === 'team1') team1.push(value);
    if (id === 'team2') team2.push(value);
    this.setState({ team1, team2 });
  }

  verifyMatch() {
    const {
      team1, team2, score1, score2,
    } = this.state;

    if (team1.length === 0
      || team2.length === 0
      || score1 === ''
      || score2 === '') return false;
    return true;
  }

  removePlayerHandler({ teamId, index }) {
    const { team1, team2 } = this.state;

    if (teamId === 1) {
      team1.splice(index, 1);
      this.setState({ team1 });
    }
    if (teamId === 2) {
      team2.splice(index, 1);
      this.setState({ team2 });
    }
  }

  scoreChangeHandler(event) {
    const { id } = event.target;
    let { value } = event.target;

    if (value !== '' && Number.isNaN(parseInt(value, 10))) return;
    if (parseInt(value, 10) >= 10000) return;
    if (parseInt(value, 10) < 0) return;
    if (value !== '') value = parseInt(value, 10);
    if (id === 'score1') this.setState({ score1: value });
    if (id === 'score2') this.setState({ score2: value });
  }

  async submitMatchHandler() {
    const {
      team1, team2, score1, score2,
    } = this.state;
    const { showNotification, updateDashboard } = this.props;

    if (this.verifyMatch() === false) {
      showNotification('Not enough information to submit', true);
      return;
    }

    const url = `${process.env.REACT_APP_API_URL}/matches`;

    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        team1, team2, score1, score2,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const result = await response.json();
    if (result.status === 'success') {
      this.setState({
        score1: '',
        score2: '',
      });
      showNotification('The match was saved', false);
      updateDashboard();
      return;
    }
    showNotification('The match was not saved. Something went wrong!', false);
  }

  render() {
    const {
      score1, score2, team2, team1,
    } = this.state;
    const { showNotification } = this.props;

    return (
      <div className="match-form">
        <h2>Add a match</h2>
        <table>
          <tbody>
            <tr className="score">
              <td className="score__left">
                <input
                  id="score1"
                  type="text" // type="number" has unwanted side effect that you
                              // cannot prevent the user from entering
                              // non-numerical characters
                  value={score1}
                  onChange={this.scoreChangeHandler}
                  autoComplete="off"
                  placeholder="0"
                  className={score1 === '' ? 'score--empty' : ''}
                />
              </td>
              <td>-</td>
              <td className="score__right">
                <input
                  id="score2"
                  type="text" // type="number" has unwanted side effect, see above
                  value={score2}
                  onChange={this.scoreChangeHandler}
                  autoComplete="off"
                  placeholder="0"
                  className={score2 === '' ? 'score--empty' : ''}
                />
              </td>
            </tr>

            <Players
              team1={team1}
              team2={team2}
              removePlayer={this.removePlayerHandler}
            />

            <Actions
              onSubmit={this.addPlayerHandler}
              setTeams={this.setTeams}
              showNotification={showNotification}
              team1={team1}
              team2={team2}
            />

            <tr className="submit">
              <td colSpan={3}>
                <button
                  type="submit"
                  className="button button--wide"
                  onClick={this.submitMatchHandler}
                  disabled={!this.verifyMatch()}
                >
                  Submit match
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

MatchForm.propTypes = {
  showNotification: PropTypes.func.isRequired,
  updateDashboard: PropTypes.func.isRequired,
};

export default MatchForm;
