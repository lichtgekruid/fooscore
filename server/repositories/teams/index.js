module.exports.getOrCreate = require('./get-or-create.js');
module.exports.getBestTeam = require('./get-best-team.js');
module.exports.getMostPlayed = require('./get-most-played.js');
module.exports.getHighscore = require('./get-highscore.js');
module.exports.get = require('./get.js');
module.exports.patchName = require('./patch-name.js');
