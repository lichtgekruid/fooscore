import React from 'react';
import PropTypes from 'prop-types';

import './notifications.css';

const Notifications = ({ notifications, deleteNotification }) => (
  <div className="notifications__container">
    <div className="notifications">
      { notifications.slice(0).reverse().map((notification, index) => (
        <div
          key={notification}
          className={`notifications__notification ${notification.warning === true ? 'notifications__notification--warning' : ''}`}
          onClick={() => deleteNotification(index)}
          type="button"
          role="button"
          tabIndex={0}
        >
          <span>{notification.message}</span>
          <span>X</span>
        </div>
      ))}
    </div>
  </div>
);

Notifications.propTypes = {
  notifications: PropTypes.array.isRequired,
  deleteNotification: PropTypes.func.isRequired,
};

export default Notifications;
