

const knex = require('libs/knex');

module.exports = async () => {
  const result = (await knex.raw(`
  SELECT id,
        name,
        (SELECT SUM(match_scores.result)
           FROM players AS players2
           JOIN team_players ON players2.id = team_players.player_id
           JOIN match_scores ON team_players.team_id = match_scores.team_id
           JOIN matches ON matches.id = match_scores.match_id
          WHERE players2.id = players.id
          AND matches.created_on > current_date - interval '7 days'
        GROUP BY name) AS score_last_week
    FROM players
GROUP BY id, name
ORDER BY score_last_week DESC
   LIMIT 1;
`)).rows[0];
  return result.name;
};
