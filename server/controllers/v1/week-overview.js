/* jshint esversion: 6 */
/* globals require, module */
const express = require('express');

const router = express.Router({ mergeParams: true });
const matchesRepo = require('repositories/matches');
const playersRepo = require('repositories/players');
const teamsRepo = require('repositories/teams');

router.get('/', async (req, res) => {
  let gamesPlayed;
  let bestTeam;
  let bestPlayer;
  let mostPlayed;

  try {
    gamesPlayed = await matchesRepo.getCount();
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err.message });
  }

  if (gamesPlayed === 0) {
    return res.status(200).json({ status: 'success', message: 'NO_DATA' });
  }

  try {
    bestPlayer = await playersRepo.getBestPlayer();
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err.message });
  }

  try {
    bestTeam = await teamsRepo.getBestTeam();
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err.message });
  }

  try {
    mostPlayed = await teamsRepo.getMostPlayed();
  } catch (err) {
    return res.status(200).json({ status: 'failed', message: err.message });
  }

  return res.status(200).json({
    status: 'success',
    data: {
      gamesPlayed,
      bestTeam,
      bestPlayer,
      mostPlayed,
    },
  });
});

module.exports = router;
