import React, { Component } from 'react';

import './dashboard.css';
import Notifications from 'components/Notifications';
import logo from 'img/fooscore-logo.svg';
import MatchForm from 'components/MatchForm';
import WeekOverview from 'components/WeekOverview';
import Ranking from 'components/Ranking';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.showNotification = this.showNotification.bind(this);
    this.deleteNotification = this.deleteNotification.bind(this);
    this.fetchTeamRanking = this.fetchTeamRanking.bind(this);
    this.update = this.update.bind(this);

    this.state = {
      notifications: [],
      teamRankings: [],
    };
  }

  componentDidMount() {
    this.fetchTeamRanking();
    this.fetchWeekOverview();
  }

  async fetchTeamRanking() {
    const url = `${process.env.REACT_APP_API_URL}/teams`;

    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const result = await response.json();

    if (result.status === 'success') {
      this.setState({
        teamRankings: result.data.rankings,
        teamHighscore: result.data.highscore,
      });
      return;
    }
    this.showNotification('Something went wrong while fetching team ranking statistics', true);
  }

  async fetchWeekOverview() {
    const url = `${process.env.REACT_APP_API_URL}/week-overview`;

    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const result = await response.json();

    if (result.status === 'success') {
      const {
        bestPlayer, bestTeam, gamesPlayed, mostPlayed,
      } = result.data;
      this.setState({
        bestPlayer,
        bestTeam,
        gamesPlayed,
        mostPlayed,
      });
      return;
    }

    this.showNotification('Something went wrong while fetching week the overview');
  }

  showNotification(message, warning = false) {
    const { notifications } = this.state;

    if (notifications.length > 1) notifications.shift();
    notifications.push({ message, warning });
    this.setState({ notifications });
  }

  deleteNotification(index) {
    const { notifications } = this.state;
    notifications.splice(index, 1);

    this.setState({ notifications });
  }

  update() {
    this.fetchTeamRanking();
    this.fetchWeekOverview();
  }

  render() {
    const {
      notifications,
      teamRankings, teamHighscore,
      bestPlayer, bestTeam, gamesPlayed, mostPlayed,
    } = this.state;
    return (
      <div className="dashboard-page">
        <Notifications
          notifications={notifications}
          deleteNotification={this.deleteNotification}
        />
        <div className="hero">
          <header>
            <a href="/">
              <img src={logo} alt="logo" className="logo" />
            </a>
          </header>
          <div className="hero__wrapper">
            <MatchForm
              showNotification={this.showNotification}
              updateDashboard={this.update}
            />
          </div>
        </div>
        <WeekOverview
          showNotification={this.showNotification}
          rankings={teamRankings}
          bestPlayer={bestPlayer}
          bestTeam={bestTeam}
          gamesPlayed={gamesPlayed}
          mostPlayed={mostPlayed}

        />
        <Ranking
          showNotification={this.showNotification}
          updateDashboard={this.update}
          rankings={teamRankings}
          highscore={teamHighscore}
        />
      </div>
    );
  }
}

export default Dashboard;
