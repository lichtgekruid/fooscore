const knex = require('libs/knex');

module.exports = async () => {
  const rankings = await knex
    .select(
      'id',
      'rank',
      'name',
      'games_played as gamesPlayed',
      'games_won as gamesWon',
      'games_drawn as gamesDrawn',
      'games_lost AS gamesLost',
      'total_score AS totalScore',
    )
    .from('team_stats')
    .orderBy('rank');

  return rankings;
};
