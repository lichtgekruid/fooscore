const knex = require('libs/knex');

module.exports = async (props) => {
  const { id, name } = props;

  await knex('teams')
    .where('id', id)
    .update({
      name,
    });
};
