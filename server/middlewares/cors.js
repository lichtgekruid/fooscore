const cors = (req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', 'true');
  res.set('Access-Control-Allow-Headers', 'Content-Type,X-Access-Token');
  res.set('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE,OPTIONS');

  if (req.method === 'OPTIONS') return res.sendStatus(200);

  return next();
};

module.exports = cors;
