import React from 'react';
import PropTypes from 'prop-types';

import './week-overview.css';

function WeekOverview(props) {
  const {
    bestPlayer, bestTeam, gamesPlayed, mostPlayed,
  } = props;

  return (
    <div className="week-overview">
      <h2>Week overview</h2>
      <div className="overview__top">
        <div className="value">{gamesPlayed}</div>
        <div className="caption">Games played</div>
      </div>
      <div className="overview__bottom">
        <div className="overview__box">
          <div className="caption">Best team</div>
          <div className="value">{bestTeam}</div>
        </div>
        <div className="overview__box">
          <div className="caption">Best player</div>
          <div className="value">{bestPlayer}</div>
        </div>
        <div className="overview__box">
          <div className="caption">Most played</div>
          <div className="value">{mostPlayed}</div>
        </div>
      </div>
    </div>
  );
}

WeekOverview.propTypes = {
  bestPlayer: PropTypes.string,
  bestTeam: PropTypes.string,
  gamesPlayed: PropTypes.number,
  mostPlayed: PropTypes.string,
};

WeekOverview.defaultProps = {
  bestPlayer: '--',
  bestTeam: '--',
  gamesPlayed: 0,
  mostPlayed: '--',
};

export default WeekOverview;
