import React from 'react';

import PropTypes from 'prop-types';
import deleteIcon from './img/delete.svg';

export default function Players(props) {
  const { team1, team2, removePlayer } = props;

  return (
    <tr className="players">
      <td>
        <ul className="players--left">
          {team1.map((player, index) => (
            <li key={player}>
              {player}
              <button
                className="players__delete"
                type="button"
                onClick={() => removePlayer({ teamId: 1, index })}
                tabIndex={0}
              >
                <img src={deleteIcon} alt="Delete player" />
              </button>
            </li>
          ))}
          { team1.length === 0 && (
            <li className="players--empty">Add players to this team</li>
          )}
        </ul>
      </td>

      <td className="players__caption">vs</td>

      <td>
        <ul className="players--right">
          {team2.map((player, index) => (
            <li key={player}>
              <button
                className="players__delete"
                type="button"
                onClick={() => removePlayer({ teamId: 2, index })}
                tabIndex={0}
              >
                <img src={deleteIcon} alt="Delete player" />
              </button>
              {player}
            </li>
          ))}
          { team2.length === 0 && (
            <li className="players--empty">Add players to this team</li>
          )}
        </ul>
      </td>

    </tr>
  );
}

Players.propTypes = {
  team1: PropTypes.array.isRequired,
  team2: PropTypes.array.isRequired,
  removePlayer: PropTypes.func.isRequired,
};
