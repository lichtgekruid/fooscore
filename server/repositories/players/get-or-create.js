/* eslint no-await-in-loop: 0 */
const knex = require('libs/knex');

module.exports = async (players) => {
  const resultPlayers = [];

  for (let i = 0; i < players.length; i += 1) {
    const name = players[i];

    await knex.raw(`
      INSERT INTO players(name)
      SELECT ?
      WHERE NOT EXISTS 
          (SELECT 1 FROM players WHERE name = ?)
    `, [name, name]);

    const player = await knex('players')
      .select('id', 'name')
      .where('name', name)
      .first();

    resultPlayers.push(player);
  }

  return resultPlayers;
};
