const knex = require('libs/knex');

module.exports = async (players) => {
  const playerIds = players.map(player => player.id);
  const [team] = await knex
    .from('teams')
    .select('teams.id')
    .count('team_players.id')
    .innerJoin('team_players', 'teams.id', 'team_players.team_id')
    .whereIn('team_players.player_id', playerIds)
    .groupBy('teams.id')
    .havingRaw('COUNT(DISTINCT team_players.player_id) = ?', playerIds.length)
    .pluck('teams.id');

  let playersInTeam;
  if (typeof team !== 'undefined') {
    playersInTeam = parseInt((await knex('team_players')
      .count('id')
      .where('team_id', team)
      .first()).count, 10);
  }

  if (typeof team !== 'undefined' && playersInTeam === players.length) return team;

  const teamName = players
    .map(player => player.name)
    .join(' + ');
  const [teamId] = await knex('teams')
    .insert({ name: teamName })
    .returning('id');
  await knex('team_players')
    .insert(players
      .map(player => ({
        team_id: teamId,
        player_id: player.id,
      })));
  return teamId;
};
