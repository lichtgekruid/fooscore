const express = require('express');

const router = express.Router();

router.use('/matches', require('./matches'));
router.use('/teams', require('./teams'));
router.use('/matchmaker', require('./matchmaker'));
router.use('/week-overview', require('./week-overview'));

module.exports = router;
