const knex = require('libs/knex');

module.exports = async ({
  team1Id, team2Id, score1, score2, result1, result2,
}) => {
  const [matchId] = await knex('matches')
    .insert({})
    .returning('id');

  await knex('match_scores')
    .insert([
      {
        match_id: matchId, team_id: team1Id, score: score1, result: result1,
      }, {
        match_id: matchId, team_id: team2Id, score: score2, result: result2,
      },
    ]);
};
