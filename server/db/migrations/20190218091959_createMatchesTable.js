exports.up = (knex, Promise) => Promise.all([
  knex.schema.createTable('matches', (table) => {
    table.increments();
    table.datetime('created_on').defaultTo(knex.fn.now());
  }),
]);

exports.down = (knex, Promise) =>
  Promise.all([
    knex.schema.dropTable('matches'),
  ]);
