import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './ranking.css';
import editIcon from './img/edit.svg';
import saveIcon from './img/save.svg';

class TeamName extends Component {
  constructor(props) {
    super(props);
    const { name } = props;

    this.changeHandler = this.changeHandler.bind(this);
    this.keyDownHandler = this.keyDownHandler.bind(this);
    this.saveHandler = this.saveHandler.bind(this);

    this.state = {
      nameValue: name,
      edit: false,
    };
  }

  async saveHandler() {
    const { nameValue } = this.state;
    const {
      id, updateDashboard, showNotification, name,
    } = this.props;

    const url = `${process.env.REACT_APP_API_URL}/teams/${id}`;
    const response = await fetch(url, {
      method: 'PATCH',
      body: JSON.stringify({
        name: nameValue,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const result = await response.json();
    if (result.status === 'success') {
      updateDashboard();
      if (nameValue === name) this.setState({ edit: false });
      return;
    }
    showNotification('Something went wrong while saving a team name', true);
  }

  keyDownHandler(event) {
    if (event.keyCode === 13) this.saveHandler();
    if (event.keyCode === 27) this.setState({ edit: false });
  }

  changeHandler(event) {
    const { value } = event.target;

    this.setState({ nameValue: value });
  }

  render() {
    const { name } = this.props;
    const { nameValue, edit } = this.state;

    if (edit === true) {
      return (
        <td
          className="rankings__team"
        >
          <button
            className="team__edit"
            onClick={this.saveHandler}
            type="button"
            tabIndex={0}
          >
            <img className="edit__img" src={saveIcon} alt="edit" />
          </button>
          <input
            type="text"
            value={nameValue}
            onChange={this.changeHandler}
            onKeyDown={this.keyDownHandler}
          />
        </td>
      );
    }

    return (
      <td
        className="rankings__team"
        onClick={() => this.setState({ edit: true })}
      >
        <img className="team__save" src={editIcon} alt="edit" />
        {name}
      </td>
    );
  }
}

TeamName.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  updateDashboard: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
};

export default TeamName;
