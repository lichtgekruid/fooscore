
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './match-form.css';

class Actions extends Component {
  constructor(props) {
    super(props);

    this.addPlayerHandler = this.addPlayerHandler.bind(this);
    this.playerValueChangeHandler = this.playerValueChangeHandler.bind(this);
    this.keyDownHandler = this.keyDownHandler.bind(this);
    this.matchMakerHandler = this.matchMakerHandler.bind(this);

    this.state = {
      player1Value: '',
      player2Value: '',
    };
  }

  playerValueChangeHandler(event) {
    const { value, id } = event.target;
    if (id === 'player1') this.setState({ player1Value: value });
    if (id === 'player2') this.setState({ player2Value: value });
  }

  addPlayerHandler(event) {
    const {
      player1Value, player2Value,
    } = this.state;
    const { onSubmit } = this.props;
    const { id } = event.target;

    if (id === 'player1-submit' || id === 'player1') {
      if (player1Value === '') return;
      onSubmit({
        id: 'team1',
        value: player1Value,
      });
      this.setState({ player1Value: '' });
    }

    if (id === 'player2-submit' || id === 'player2') {
      if (player2Value === '') return;
      onSubmit({
        id: 'team2',
        value: player2Value,
      });
      this.setState({ player2Value: '' });
    }
  }

  keyDownHandler(event) {
    if (event.keyCode === 13) this.addPlayerHandler(event);
  }

  async matchMakerHandler() {
    const {
      team1, team2, setTeams, showNotification,
    } = this.props;
    const playerNames = team1.concat(team2);

    if (playerNames.length <= 1) {
      showNotification('The matchmaker needs 2 or more player names', true);
      return;
    }

    const url = `${process.env.REACT_APP_API_URL}/matchmaker`;
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        playerNames,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const result = await response.json();
    if (result.status === 'success') {
      setTeams(result.data.team1, result.data.team2);
      showNotification('Players are matched up. A match made in... the server');
      return;
    }
    showNotification('Something went wrong with the matchmaker', true);
  }

  render() {
    const {
      player1Value, player2Value,
    } = this.state;

    return (
      <tr className="actions">
        <td className="actions__left">
          <div className="player-form__container">
            <input
              placeholder="Player name"
              id="player1"
              onChange={this.playerValueChangeHandler}
              value={player1Value}
              onKeyDown={this.keyDownHandler}
            />
            <button
              id="player1-submit"
              type="submit"
              onClick={this.addPlayerHandler}
              className="button button--ghost player-form__add-button"
            >
              <span className="desktop">Add</span>
              <span className="mobile">+</span>
            </button>
          </div>
        </td>

        <td className="actions__matchmaker">
          <button
            onClick={this.matchMakerHandler}
            className="button button--ghost"
            type="button"
          >
            Match
            <span className="desktop">maker</span>
          </button>
        </td>

        <td>
          <div className="player-form__container">
            <input
              placeholder="Player name"
              id="player2"
              onChange={this.playerValueChangeHandler}
              value={player2Value}
              onKeyDown={this.keyDownHandler}
            />
            <button
              id="player2-submit"
              type="submit"
              onClick={this.addPlayerHandler}
              className="button button--ghost player-form__add-button"
            >
              <span className="desktop">Add</span>
              <span className="mobile">+</span>
            </button>
          </div>
        </td>
      </tr>
    );
  }
}

Actions.propTypes = {
  team1: PropTypes.array.isRequired,
  team2: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  setTeams: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
};

export default Actions;
