const knex = require('libs/knex');

module.exports = async () => {
  const result = (await knex.raw(`
  SELECT id, name,
         (SELECT COUNT(match_scores.id)
            FROM match_scores
            JOIN matches ON matches.id = match_scores.match_id
           WHERE match_scores.team_id = teams.id
             AND matches.created_on > current_date - interval '7 days'
        GROUP BY name) AS team_score
    FROM teams
GROUP BY id, name
ORDER BY team_score DESC
   LIMIT 1;
`)).rows[0];
  return result.name;
};
