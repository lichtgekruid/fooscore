exports.up = (knex, Promise) => Promise.all([
  knex.schema.createTable('match_scores', (table) => {
    table.increments();
    table.integer('match_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('matches');

    table.integer('team_id')
      .unsigned()
      .index()
      .references('id')
      .inTable('teams');
    table.integer('score');
    table.integer('result');
  }),
]);

exports.down = (knex, Promise) =>
  Promise.all([
    knex.schema.dropTable('match_scores'),
  ]);
