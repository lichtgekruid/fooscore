const totalScoreSQL = `
 (SELECT SUM(match_scores.result)
    FROM match_scores
   WHERE match_scores.team_id = teams.id
GROUP BY match_scores.team_id)`;

const gamesPlayedSQL = `
 (SELECT COUNT(match_scores.result)
    FROM match_scores
   WHERE match_scores.team_id = teams.id
GROUP BY match_scores.team_id)`;

exports.up = (knex, Promise) => Promise.all([
  knex.schema.raw(`
    CREATE VIEW team_stats AS
    SELECT
        teams.id,
        rank() OVER (
               ORDER BY ${totalScoreSQL} DESC, ${gamesPlayedSQL}
          ) AS rank,
        teams.name,
        ${gamesPlayedSQL} AS games_played,
        COALESCE((SELECT COUNT(match_scores.result)
                    FROM match_scores
                   WHERE match_scores.team_id = teams.id
                     AND match_scores."result" = 2
                GROUP BY match_scores.team_id), 0) AS games_won,
        COALESCE((SELECT COUNT(match_scores.result)
                    FROM match_scores
                   WHERE match_scores.team_id = teams.id
                     AND match_scores."result" = 1
                GROUP BY match_scores.team_id), 0) AS games_drawn,
        COALESCE((SELECT COUNT(match_scores.result)
                    FROM match_scores
                   WHERE match_scores.team_id = teams.id
                     AND match_scores."result" = 0
                GROUP BY match_scores.team_id), 0) AS games_lost,
        ${totalScoreSQL} AS total_score
      FROM teams;`),
]);

exports.down = (knex, Promise) =>
  Promise.all([
    knex.schema.raw('DROP VIEW team_stats;'),
  ]);
